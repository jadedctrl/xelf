;; Defining a loadable system: the file PLONG.ASD

;; We must create a small .ASD file with your project's name and source
;; file.

;; #+name: plong.asd

(asdf:defsystem #:plong
  :depends-on (:xelf)
  :components ((:file "plong")))
