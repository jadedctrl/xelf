* TODO prevent multi-kick from holding button 
* TODO use pathfinding to optimize AI barrier attack plan
* TODO allow different foozpong/2600combat like configurations of vertical and horizontal bars
** TODO reset to change config in barrier mode

* DONE fix ball getting stuck on walls
  CLOSED: [2017-03-25 Sat 17:32]
* DONE fix pathfinding hitting corners
  CLOSED: [2017-03-25 Sat 17:32]
* DONE use two feelers instead of one to steer away from walls
  CLOSED: [2017-03-27 Mon 08:14]
* DONE improve AI with pathfinding
  CLOSED: [2017-03-24 Fri 23:52]
** DONE deactivate existing barrier waypoints
   CLOSED: [2017-03-24 Fri 23:52]
** DONE splice in pathfinder waypoints
   CLOSED: [2017-03-24 Fri 23:52]
** DONE test various grid sizes
   CLOSED: [2017-03-24 Fri 23:52]
* DONE barrier mode default
  CLOSED: [2017-03-24 Fri 23:52]
* DONE enlarge hitbox of ball somewhat
  CLOSED: [2017-03-23 Thu 07:39]
* DONE allow more destruction per sortie into fortress
  CLOSED: [2017-03-23 Thu 07:39]
* DONE increase time limit of fortress game
  CLOSED: [2017-03-23 Thu 07:39]
* DONE tighter inertia turning radius
  CLOSED: [2017-03-23 Thu 07:39]
* DONE reduce wobble angle 
  CLOSED: [2017-03-23 Thu 07:39]
* DONE fix game reset hanging after config
  CLOSED: [2017-03-21 Tue 20:31]
* DONE fix terminal not appearing in 3x0ng
  CLOSED: [2017-03-21 Tue 21:10]
* DONE fix 3x0ng graphical glitch
  CLOSED: [2017-03-21 Tue 20:06]
