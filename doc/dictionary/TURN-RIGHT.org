#+OPTIONS: *:nil
** TURN-RIGHT (generic function)
 
: (NODE RADIANS)
Decrease heading by RADIANS.
