#+OPTIONS: *:nil
** HANDLE-COLLISION (generic function)
 
: (NODE OTHER-NODE)
Wraparound for collision handling. You shouldn't need to use this
explicitly.
