#+OPTIONS: *:nil
** RESIZE-TO-BACKGROUND-IMAGE (generic function)
 
: (BUFFER)
Resize the buffer to fit its background image.
