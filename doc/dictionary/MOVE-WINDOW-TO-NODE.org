#+OPTIONS: *:nil
** MOVE-WINDOW-TO-NODE (generic function)
 
: (BUFFER NODE)
Move the buffer's window to the node NODE.
