#+OPTIONS: *:nil
** DRAW-IMAGE (function)
 
: (NAME X Y &KEY (Z 0.0) (BLEND :ALPHA) (OPACITY 1.0) HEIGHT WIDTH)
Draw the image named NAME at x,y,z, sized HEIGHT, WIDTH, with blending mode BLEND.
