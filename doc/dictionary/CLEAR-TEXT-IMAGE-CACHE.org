#+OPTIONS: *:nil
** CLEAR-TEXT-IMAGE-CACHE (function)
 
: (&KEY (DELETE-TEXTURES T))
Free up texture memory used by rendered outline fonts.
