#+OPTIONS: *:nil
** COMPOSE-BELOW (function)
 
: (&OPTIONAL BUFFER1 BUFFER2)
Return a new buffer containing all the objects from BUFFER1 and
BUFFER2, with BUFFER2's objects pasted below those of BUFFER1. The
original buffers are destroyed.
