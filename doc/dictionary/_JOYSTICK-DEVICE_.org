#+OPTIONS: *:nil
** *JOYSTICK-DEVICE* (variable)
The SDL device id of the current joystick.
