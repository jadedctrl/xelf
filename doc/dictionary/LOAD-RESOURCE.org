#+OPTIONS: *:nil
** LOAD-RESOURCE (function)
 
: (RESOURCE)
Load the driver-dependent object of RESOURCE into the OBJECT field
so that it can be fed to the console.
