#+OPTIONS: *:nil
** QUIT (function)
 
: (&OPTIONAL SHUTDOWN)
Exit the game engine.
