#+OPTIONS: *:nil
** *GL-SCREEN-HEIGHT* (variable)
Height of the window expressed in OpenGL coordinates.
