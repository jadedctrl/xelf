#+OPTIONS: *:nil
** WITH-QUADTREE (macro)
 
: (QUADTREE &BODY BODY)
Evaluate BODY forms with *QUADTREE* bound to QUADTREE.
