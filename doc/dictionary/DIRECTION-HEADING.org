#+OPTIONS: *:nil
** DIRECTION-HEADING (function)
 
: (DIRECTION)
Return the angle (in radians) of the keyword DIRECTION.
