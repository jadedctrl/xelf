#+OPTIONS: *:nil
** AIM-AT (generic function)
 
: (NODE OTHER-NODE)
Set the NODE's heading to aim at the OTHER-NODE.
