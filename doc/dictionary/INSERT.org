#+OPTIONS: *:nil
** INSERT (generic function)
 
: (NODE &OPTIONAL X Y Z)
Add the NODE to the current buffer, optionally at X,Y,Z.
