#+OPTIONS: *:nil
** DEFRESOURCE (macro)
 
: (&REST ENTRIES)
Define a new resource.

A Xelf 'resource' is an image, sound, text, color, or font. Most
resources will depend on a file for their data, such as a .PNG file
for images and .WAV files for sounds.

A 'resource record' defines a resource. A resource record is a
property list with the following elements:

 - :NAME    A string; the name of the resource. (Required)
 - :TYPE    A keyword symbol identifying the data type.
            Valid types are :color :music :image :sample :ttf :font
            If TYPE is not given, Xelf will try to guess the file type
            based on the extension given as the NAME.
 - :PROPERTIES  Property list with extra data specific to resource TYPE.
 - :FILE    Name of file to load data from, if any. 
            Relative to project directory.
            If FILE is not given, use the NAME.

