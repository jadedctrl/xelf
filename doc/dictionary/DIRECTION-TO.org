#+OPTIONS: *:nil
** DIRECTION-TO (generic function)
 
: (NODE OTHER-NODE)
Returns the approximate keyword compass direction between NODE and OTHER-NODE.
