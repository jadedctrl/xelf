#+OPTIONS: *:nil
** FONT-WIDTH (function)
 
: (FONT)
Character with of a bitmap font FONT.
Signals an error when called on an outline font.
