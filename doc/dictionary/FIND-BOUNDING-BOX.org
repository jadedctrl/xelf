#+OPTIONS: *:nil
** FIND-BOUNDING-BOX (function)
 
: (NODES)
Return as multiple values the minimal bounding box 
containing the NODES.
