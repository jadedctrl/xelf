#+OPTIONS: *:nil
** *SCALE-OUTPUT-TO-WINDOW* (variable)
When non-nil, always show a fixed amount of the buffer when changing
window size. Otherwise (the default) one onscreen pixel equals one
unit of buffer space, so that more of the buffer shows if the window
becomes larger.
