#+OPTIONS: *:nil
** KEYBOARD-KEYS-DOWN (function)
Returns a list of the keys that are depressed.
