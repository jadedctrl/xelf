#+OPTIONS: *:nil
** DRAW-TEXTURED-RECTANGLE-* (function)
 
: (X Y Z WIDTH HEIGHT TEXTURE &KEY U1 V1 U2 V2 (WINDOW-X (WINDOW-X))
     (WINDOW-Y (WINDOW-Y)) ANGLE (BLEND :ALPHA) (OPACITY 1.0)
     (VERTEX-COLOR "white"))
Draw an OpenGL textured rectangle at X, Y, Z with width WIDTH and height HEIGHT.
The argument TEXTURE is a string image name (or a texture returned by
FIND-TEXTURE). BLEND sets the blending mode and can be one
of :ALPHA, :ADDITIVE, :MULTIPLY. OPACITY is 1.0 for opaque, 0.0 for transparent.
