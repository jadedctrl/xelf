#+OPTIONS: *:nil
** MESSAGE (function)
 
: (FORMAT-STRING &REST ARGS)
Print a log message by passing the arguments to
`*message-function'. When the variable `*message-logging*' is nil,
this output is disabled.
