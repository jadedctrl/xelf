#+OPTIONS: *:nil
** HEADING-BETWEEN (generic function)
 
: (NODE OTHER-NODE)
Return the angle (in radians) of the ray from NODE to OTHER-NODE.
