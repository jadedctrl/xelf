#+OPTIONS: *:nil
** MAKE-EVENT (function)
 
: (CODE MODIFIERS)
Create an input event for the key CODE with MODIFIERS pressed.
The argument CODE may be one of:

   - a keyword symbol naming the keyboard key, such as :RETURN or :SPACE
     (see also `make-key-symbol'.)

   - a one-character string, whose first character is the translated
     Unicode character being bound

   - an integer whose value is the unicode character code from SDL

or, 

   - a cons of the form (key unicode) will be passed through
     unaltered.
