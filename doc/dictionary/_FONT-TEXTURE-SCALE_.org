#+OPTIONS: *:nil
** *FONT-TEXTURE-SCALE* (variable)
Scaling factor for rendering of outline fonts.
Use this when your game window might be enlarged to the point of
blurring font textures that are too small.
