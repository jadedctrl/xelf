#+OPTIONS: *:nil
** DRAW (generic function)
 
: (NODE)
Draw this node as a sprite. By default only %IMAGE is drawn.
The following node fields will control sprite drawing:

   %OPACITY  Number in the range 0.0-1.0 with 0.0 being fully transparent
             and 1.0 being fully opaque.

   %BLEND    Blending mode for OpenGL compositing.
             See the function `set-blending-mode' for a list of modes.
