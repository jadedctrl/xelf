#+OPTIONS: *:nil
** OPPOSITE-HEADING (function)
 
: (HEADING)
Return the heading angle opposite to HEADING.
