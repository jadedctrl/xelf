#+OPTIONS: *:nil
** FIND-RESOURCE-PROPERTY (function)
 
: (RESOURCE-NAME PROPERTY)
Read the value of PROPERTY from the resource RESOURCE-NAME.
