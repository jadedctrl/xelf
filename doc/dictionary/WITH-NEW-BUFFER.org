#+OPTIONS: *:nil
** WITH-NEW-BUFFER (macro)
 
: (&BODY BODY)
Evaluate the BODY forms in a new buffer.
