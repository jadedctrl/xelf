#+OPTIONS: *:nil
** PASTE (function)
 
: (DESTINATION SOURCE &OPTIONAL (DX 0) (DY 0))
Copy the objects in SOURCE into DESTINATION with offset DX,DY.
