#+OPTIONS: *:nil
** RADIAN-ANGLE (function)
 
: (DEGREES)
Convert DEGREES to radians.
