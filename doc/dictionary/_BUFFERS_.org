#+OPTIONS: *:nil
** *BUFFERS* (variable)
When non-nil, the UUID of the current buffer object.
