#+OPTIONS: *:nil
** DISTANCE (function)
 
: (X1 Y1 X2 Y2)
Compute the distance between the points X1,Y1 and X2,Y2.
