#+OPTIONS: *:nil
** FIND-RESOURCE-OBJECT (function)
 
: (NAME &OPTIONAL NOERROR)
Obtain the resource object named NAME, or signal an error if not
found.
