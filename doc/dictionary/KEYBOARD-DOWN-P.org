#+OPTIONS: *:nil
** KEYBOARD-DOWN-P (function)
 
: (KEY)
Returns t if the KEY is depressed.
