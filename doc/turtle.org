#+TITLE: Turtle graphics GUI demo in Xelf
#+OPTIONS: toc:3 *:nil
#+PROPERTY: header-args:lisp :results silent :noweb yes :tangle ../turtle/turtle.lisp
#+INFOJS_OPT: view:info mouse:underline up:index.html home:http://xelf.me toc:t ftoc:t ltoc:t

* Overview 

#+BEGIN_QUOTE
Use the N and P keys to flip to the Next and Previous pages, or click
the links in the header. Press B to go Back or "?" for help. This
document can also be browsed as a [[file:turtle-flat.html][single large web page]] without
Javascript.
#+END_QUOTE

In this document we build a small interactive demo of Xelf's GUI
toolkit (see also the section "Graphical User Interface" on [[http://xelf.me/xelf.html][the Xelf
core source code page.]])

This document is part of [[file:guide.html][Dave's guide to Common Lisp game
development]]. It will help to have completed reading the "2D Sprites
with Xelf" example on that page before continuing.

* System and package definitions

  As usual we must write TURTLE.ASD:

#+begin_src lisp :tangle ../turtle/turtle.asd
 (asdf:defsystem #:turtle
  :depends-on (:xelf)
  :components ((:file "turtle")))
#+end_src

  Then in our main source file:

#+begin_src lisp
(defpackage #:turtle
  (:use #:cl #:xelf)
  (:export turtle))

(in-package :turtle)
#+end_src

  
